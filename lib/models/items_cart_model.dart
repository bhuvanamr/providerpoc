import 'package:flutter/cupertino.dart';

class CartItems {
  int id;
  IconData icon;
  String itemName;
  Color color;
  String features;

  CartItems({this.itemName, this.icon, this.id, this.color, this.features});
}
