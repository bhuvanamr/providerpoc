import 'package:flutter/cupertino.dart';
import 'package:providerpoc/models/items_cart_model.dart';

class AppState with ChangeNotifier {
  String _displayText = "";

  void setDisplayText(String text) {
    _displayText = text;
    notifyListeners();
  }

  String get getDisplayText => _displayText;

  int count = 0;

  void incrementCounter() {
    count++;
    notifyListeners();
  }

  int _tapIndex = 0;

  void onTapChanged(int index) {
    _tapIndex = index;
    notifyListeners();
  }

  int get getOnTap => _tapIndex;

  CartItems _cartItems = CartItems();

  get cartItems {
    return _cartItems;
  }

  set cartItems(value) {
    _cartItems = value;
    notifyListeners();
  }

  List<String> _animalName= [];
  Future<List<String>> getAnimalNames(value) async {
    _animalName.addAll(value);
    print("names $_animalName");
    await new Future.delayed(new Duration(seconds: 5));

    return _animalName;
  }
}
