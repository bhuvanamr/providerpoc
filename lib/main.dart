import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providerpoc/appState.dart';
import 'package:providerpoc/providers/easy.dart';
import 'package:providerpoc/providers/hard.dart';
import 'package:providerpoc/providers/medium.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AppState>(
      create: (_) => AppState(),
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          home: MyHomePage(),
          ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("PROVIDERS",style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold)),
          backgroundColor: Colors.deepPurpleAccent,
          centerTitle: true,
        ),
        body: Container(
//        color: Colors.purple,
          margin: EdgeInsets.all(30.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                commonButton("EASY",(){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => Easy()));
                }),
                SizedBox(height: 30.0,),
                commonButton("MEDIUM",(){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => Medium()));
                }),
                SizedBox(height: 30.0,),
                commonButton("HARD",(){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => Hard()));
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
  Widget commonButton(String title, Function button){
    return Container(
      width: 800.0,
      child: RaisedButton(
        padding: EdgeInsets.all(10.0),
        color: Colors.deepPurpleAccent,
        onPressed: () {
          button();
        },
        child: Text(title,style: TextStyle(fontSize: 20,color: Colors.white),),
      ),
    );
  }
}


//class MyHomePage extends StatefulWidget {
//  MyHomePage({Key key}) : super(key: key);
//
//  @override
//  _MyHomePageState createState() => _MyHomePageState();
//}
//
//class _MyHomePageState extends State<MyHomePage> {
//  @override
//  Widget build(BuildContext context) {
//    return SafeArea(
//      child: Scaffold(
//        appBar: AppBar(
//          title: Text("PROVIDERS",style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold)),
//          backgroundColor: Colors.deepPurpleAccent,
//          centerTitle: true,
//        ),
//        body: Container(
////        color: Colors.purple,
//          margin: EdgeInsets.all(30.0),
//          child: SingleChildScrollView(
//            child: Column(
//              mainAxisAlignment: MainAxisAlignment.start,
//              children: <Widget>[
//                commonButton("EASY",(){
//                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => Easy()));
//                }),
//                SizedBox(height: 30.0,),
//                commonButton("MEDIUM",(){
//                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => Medium()));
//                }),
//                SizedBox(height: 30.0,),
//                commonButton("HARD",(){
//                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => Hard()));
//                }),
//              ],
//            ),
//          ),
//        ),
//      ),
//    );
//  }
//  Widget commonButton(String title, Function button){
//    return Container(
//      width: MediaQuery.of(context).size.width * 1,
//      child: RaisedButton(
//        padding: EdgeInsets.all(10.0),
//        color: Colors.deepPurpleAccent,
//        onPressed: () {
//          button();
//        },
//        child: Text(title,style: TextStyle(fontSize: 20,color: Colors.white),),
//      ),
//    );
//  }
//}
