import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providerpoc/appState.dart';

class Hard extends StatefulWidget {
  @override
  _HardState createState() => _HardState();
}

class _HardState extends State<Hard> {
  List<String> animals = [
    "Horses",
    "Tigers",
    "Lions",
    "Chickens",
    "Elephants",
    "Monkeys",
    "Deers",
    "Kangaroos",
  ];

  @override
  Widget build(BuildContext context) {
    AppState names = Provider.of<AppState>(context);
    var streamBuilder = new StreamBuilder(
      stream: names.getAnimalNames(animals).asStream(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        print('Has error: ${snapshot.hasError}');
        print('Has data: ${snapshot.hasData}');
        print('Snapshot Data ${snapshot.data}');
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return Center(
                child: new CircularProgressIndicator(
                    backgroundColor: Colors.white,
                    valueColor:
                        new AlwaysStoppedAnimation<Color>(Colors.deepPurple),
                    strokeWidth: 7));
          default:
            if (snapshot.hasError)
              return new Text('Error: ${snapshot.error}');
            else
              return createListView(context, snapshot);
        }
      },
    );

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          "StreamBuilder",
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        backgroundColor: Colors.deepPurple,
      ),
      body: streamBuilder,
    );
  }

  Widget createListView(BuildContext context, AsyncSnapshot snapshot) {
    List<String> values = snapshot.data;
    return new ListView.builder(
      itemCount: values.length,
      itemBuilder: (BuildContext context, int index) {
        return new Column(
          children: <Widget>[
            new ListTile(
              title: new Text(values[index]),
            ),
            new Divider(
              height: 5.0,
            ),
          ],
        );
      },
    );
  }
}

//class Medium extends StatelessWidget {
//  StreamBuilder<int> streamBuilder;
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      color: Colors.white,
//      child: Center(
//        child: StreamBuilder(
//          builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
//            if (snapshot.connectionState == ConnectionState.done) {
//              return Text(
//                '1 Minute Completed',
//                style: TextStyle(
//                  fontSize: 30.0,
//                ),
//              );
//            } else if (snapshot.connectionState == ConnectionState.waiting) {
//              return Text(
//                'Waiting For Stream',
//                style: TextStyle(
//                  fontSize: 30.0,
//                ),
//              );
//            }
//            return Text(
//              '00:${snapshot.data.toString().padLeft(2, '0')}',
//              style: TextStyle(
//                fontSize: 30.0,
//              ),
//            );
//          },
//          initialData: 0,
//          stream: _stream(),
//        ),
//      ),
//    );
//  }
//
//  Stream<int> _stream() {
//    Duration interval = Duration(seconds: 1);
//    Stream<int> stream = Stream<int>.periodic(interval, transform);
//    stream = stream.take(59);
//    return stream;
//  }
//
//  int transform(int value) {
//    return value;
//  }
//}
