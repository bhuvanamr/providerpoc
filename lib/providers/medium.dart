import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providerpoc/appState.dart';
import 'package:providerpoc/models/items_cart_model.dart';


class Medium extends StatelessWidget {
  PageController pageController = PageController();
  CartItems _cartItems = CartItems();
  List<CartItems> cartItems = [
    CartItems(
        itemName: "Mobile",
        icon: Icons.phone_android,
        id: 1,
        color: Colors.deepPurple,
        features:
            "6.55 Inch AMOLED 2400 x 1080 Display\n48 MP, 12 MP, 16 MP Primary Camera"),
    CartItems(
        itemName: "IPhone",
        icon: Icons.phone_iphone,
        id: 2,
        color: Colors.amber,
        features:
            " Dual SIM (nano-SIM and eSIM)12 MP Rear Camera, 7 MP Selfie Camera\nFingerprint-resistant Oleophobic Coating"),
    CartItems(
        itemName: "Sony TV",
        icon: Icons.tv,
        id: 3,
        color: Colors.green,
        features:
            "Resolution: Full HD (1920x1080)\nDisplay: Clear Resolution Enhancer"),
    CartItems(
        itemName: "HeadSet",
        icon: Icons.headset,
        id: 4,
        color: Colors.blue,
        features:
            "Active noise cancelling, 32 mm drivers\nHands-free calls, 2 hrs recharging time"),
    CartItems(
        itemName: "Camera",
        icon: Icons.photo_camera,
        id: 5,
        color: Colors.greenAccent,
        features:
            "Define every detail with its 39-point Autofocus\nEnjoy greater resolution in any light"),
    CartItems(
        itemName: "Printer",
        icon: Icons.print,
        id: 6,
        color: Colors.lightGreenAccent,
        features:
            "Automatic paper sensor, borderless printing\nUp to 1000 pages of monthly duty cycle"),
    CartItems(
        itemName: "SD card",
        icon: Icons.sd_storage,
        id: 7,
        color: Colors.pink,
        features:
            "Quick transfer speeds upto 100 MB/sec\n1920 x 1080 Full HD video support"),
  ];

  @override
  Widget build(BuildContext context) {
    final _items = Provider.of<AppState>(context, listen: false);
    final pageIndex = Provider.of<AppState>(context);
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 20,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            title: Text(
              "MEDIUM",
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            centerTitle: true,
            actions: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: Center(
                  child: Stack(
                    children: <Widget>[
                      Icon(Icons.shopping_cart, color: Colors.white, size: 40),
                      Positioned(
                        bottom: 0.0,
                        right: 0.0,
                        child: Container(
                          height: 22,
                          width: 22,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15.0),
                              color: Colors.redAccent),
                          child: Center(
                              child: Consumer<AppState>(
                            builder: (context, appState, child) => Text(
                              _items.count.toString(),
                              style:
                                  TextStyle(color: Colors.yellow, fontSize: 16),
                            ),
                          )),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          body: PageView(
            onPageChanged: (num) {
              print("num $num");
              pageIndex.onTapChanged(num);
              print("onTapChanged ${pageIndex.getOnTap}");
            },
            physics: NeverScrollableScrollPhysics(),
            scrollDirection: Axis.vertical,
            controller: pageController,
            children: <Widget>[
              ListView.builder(
                  itemCount: cartItems.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        print("${cartItems[index].itemName}");
                        pageController.jumpToPage(cartItems[index].id);
                        _cartItems = cartItems[index];
                      },
                      child: commonCard(
                          cartItems[index].icon, cartItems[index].itemName, () {
                        _items.incrementCounter();
                      }, cartItems[index].color),
                    );
                  }),
              detailsTopView(() {
                _items.incrementCounter();
              }),
            ],
          )),
    );
  }

  Widget commonCard(
      IconData iconData, String itemTitle, Function addFunc, Color color) {
    return Container(
      height: 60.0,
      margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 30.0),
      child: Card(
        elevation: 3,
        color: color.withOpacity(0.5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Icon(
              iconData,
              size: 40,
            ),
            Text(
              itemTitle,
              style: TextStyle(fontSize: 20),
            ),
            GestureDetector(
                onTap: () {
                  addFunc();
                },
                child: Icon(
                  Icons.add,
                  size: 30,
                ))
          ],
        ),
      ),
    );
  }

  Widget detailPage(IconData icons, String titles, Function addCart) {
    return Container(
      child: Column(
        children: <Widget>[
          Icon(
            icons,
            size: 40,
          ),
          Text(
            titles,
            style: TextStyle(fontSize: 20),
          ),
          GestureDetector(
              onTap: () {
                addCart();
              },
              child: Icon(
                Icons.add,
                size: 30,
              ))
        ],
      ),
    );
  }

  Widget detailsTopView(Function addCart) {
    return Column(
      children: <Widget>[
        Container(
          alignment: Alignment.centerLeft,
          color: Colors.transparent,
          child: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            iconSize: 25,
            onPressed: () {
              pageController.jumpToPage(0);
            },
          ),
        ),
        Column(
          children: <Widget>[
            Icon(
              _cartItems.icon ?? null,
              size: 180,
              color: Colors.grey,
            ),
            SizedBox(height: 10.0),
            Text(
              _cartItems.itemName ?? "",
              style: TextStyle(
                  fontSize: 20,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.bold,
                  color: Colors.black87),
            ),
            SizedBox(height: 20.0),
            Container(
                margin: EdgeInsets.symmetric(horizontal: 50.0),
                child: Text(
                  _cartItems.features ?? "",
                  style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic),
                  textAlign: TextAlign.center,
                )),
            SizedBox(height: 20.0),
            GestureDetector(
              onTap: () {
                print("AddCart");
                addCart();
              },
              child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    color: Colors.pink,
                  ),
                  width: 130.0,
                  height: 45.0,
                  margin: EdgeInsets.symmetric(horizontal: 30.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Text(
                        "Add to Cart",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                      Icon(
                        Icons.add,
                        color: Colors.white,
                        size: 25.0,
                      )
                    ],
                  )),
            )
          ],
        )
      ],
    );
  }
}
